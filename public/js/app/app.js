window.__vue_data.tab =  1;
window.__vue_data.MaturityTab =  'Maintainability';
window.__vue_data.feedBack = {
	1: '',
	2: true,
	3: '',
	4: '',
	5: '',
	6: '',
}
window.__vue_data.feedBack.repo = window.__vue_data.repo.name
window.__vue_data.feedBack.sent = 0;

var xmlhttp = new XMLHttpRequest();



var App = new Vue({
	el: '#app',
	data: window.__vue_data,


	methods:{
		getFileInfo: function(){},
		foundLicense: function(){return !this.repo.license==null;},

		sendFeedback:function(){
			var http = new XMLHttpRequest();	
			http.open('POST', '/feedback');
			http.setRequestHeader("Content-Type", "application/json");		
			http.setRequestHeader('X-CSRF-TOKEN', document.getElementsByName('csrf-token')[0].content);
			http.send(JSON.stringify( {data: this.feedBack} ));
			
			this.feedBack.sent = 1;
		}

	},

	computed: {
		isLicenseViral: function(){

			if (this.repo.license==null )
				return false;

			if (this.repo.license.name.toLowerCase().indexOf('lgpl')>=0){
				isViral = false;
			} else if  (this.repo.license.name.toLowerCase().indexOf('lesser')>=0){
				isViral = false;

			} else if  (this.repo.license.name.toLowerCase().indexOf('gnu')>=0){
				isViral = true;

			} else if  (this.repo.license.name.toLowerCase().indexOf('gpl')>=0){
				isViral = true;

			} else {
				isViral = false;
			}

			return isViral;
		}
	}
});


// xmlhttp.open("GET", '/ajax/file-info', true);
// xmlhttp.send();