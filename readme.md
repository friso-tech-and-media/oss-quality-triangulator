# OSS Quality Triangulator

## Theory

## Installation
```bash
	git clone https://gitlab.com/Arevico/oss-quality-triangulator.git ./oss-quality/
	cd ./oss-quality/
	composer install
	php artisan migrate
```

## Usage
Use `php artisan serve` to fire up the build-in php webserver.

The tool is now available to you on `http://localhost:8000/`