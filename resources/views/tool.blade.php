<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <meta charset="utf-8">	
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{{ Session::token() }}}">

        <title>OSS Quality Triangulator</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

		<!-- Scripts -->
		<script src="{{ URL::asset('js/app/vue.js') }}"></script>

		<!-- Styles -->
		<link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

		<style>
			input[type="text"]{
				width:400px;
				text-align:center;
				/* padding:10px; */
			}

			[v-cloak] {display: none}
		</style>
    </head>
    <body>
        <div class="full-height">
                <div class="top-right links">
					<a href="">Theory</a>
					<a href="https://www.linkedin.com/in/frisokluitenberg/">Contact Me</a>
                </div>

            <div class="content">
                <div class="title m-b-md m-t-md">
                    OSS Quality Triangulator
                </div>
				<!-- Tool Content -->
					<!-- <h2> - Getting Started - </h2>
					<div class="explain">
						This tool will assess the quality of Open Source Projects on GitHub. In addition, data from sources such as Stackexchange will be requested aswell. To start, enter the url to a repository to evaluate and click '- Start -'
					</div> -->
				<div>
					<form method="GET" action="{{ route('tool') }}">
						<input type="text" name="url" id="" placeholder="http://github.com/vuejs/vue" value="{{ $_GET['url'] }}">
						<input type="submit" value=" - Start - ">
					</form>
							
					<br />&nbsp;
					<br />&nbsp;
				</div>
				

				<div id="app" >
					<!-- Left Sidebar -->
					<div class="sidebar links" v-cloak>
						<h2 class="h-c">Select</h2>
						<a @click.prevent.stop="tab=1" href="">1. Basic Information</a><br />
						<a @click.prevent.stop="tab=2" href="">2. Component Maturity</a><br />
						<a @click.prevent.stop="tab=3" href="">3. Risk Analysis</a><br /> &nbsp;<br />
						<a @click.prevent.stop="tab=4" href="">4. View Commits</a><br />
						<a @click.prevent.stop="tab=5" href="">5. View Issues</a><br /> &nbsp;<br />
						<a @click.prevent.stop="tab=6" href="">6. feedback</a><br />
						<a @click.prevent.stop="tab=7" href="">7. Contribute</a><br />
						
						<br />
					</div>
					<!-- /Left SideBar -->
					
					<!-- Main windows-->
					<div class="main" v-cloak>
						<div v-show="tab==1">
							<h2 class="h-c">Repository & Owner</h2>
							@include('tab.BasicInfo')
						</div>

						<div v-show="tab==2">
							<h2 class="h-c">Component Maturity</h2>
							@include('tab.Maturity')

						</div>

						<div v-show="tab==3">
							<h2 class="h-c">Risk Analysis</h2>
							@include('tab.Risk')

						</div>
						
						<div v-show="tab==4">
							<h2 class="h-c">Commits</h2>
							@include('tab.Commits')
						</div>

						<div v-show="tab==5">
							<h2 class="h-c">Issues</h2>
							@include('tab.Issues')
	
						</div>
			
						<div v-show="tab==6">
							<h2 class="h-c">Feedback</h2>
							@include('tab.Feedback');
						</div>

						<div v-show="tab==7">
							<h2 class="h-c">Contribute</h2>
							@include('tab.Contribute');
						</div>

					</div>
				<!-- /Main window -->
				</div>
				<!-- / End tool-->
			</div>	

         </div>

		<!-- Server Data -->
		<script>
			window.__vue_data = {!! $data !!};
			
		</script>

		<!-- Renderer -->
		 <script src="{{ URL::asset('js/app/app.js') }}"></script>

    </body>
</html>
