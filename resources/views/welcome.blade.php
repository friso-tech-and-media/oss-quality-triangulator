<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{{ Session::token() }}}">
		
        <title>OSS Quality Triangulator</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
		<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
		<style>
			input[type="text"]{
				width:400px;
				text-align:center;
	
			}
		</style>
    </head>
    <body>	
        <div class="flex-center position-ref full-height">
                <div class="top-right links">
					<a href="">Theory</a>
					<a href="https://www.linkedin.com/in/frisokluitenberg/">Contact Me</a>
                </div>

            <div class="content">
                <div class="title m-b-md">
                    OSS Quality Triangulator
                </div>

                <div class="links">
                    <a href="#">1. Theory</a>
                    <a href="#">2. Maturity Score</a>
                    <a href="#">3. Risk Analysis</a>
                    <a href="#">4. Feedback</a>
                    <a href="#">5. Collaborate</a>
				</div>
				
				<div class="links">	
				<br />&nbsp;
				<br />
					<form name="tool" method="GET" action="{{ route('tool') }}">
						<input type="text" name="url" value="https://github.com/laravel/laravel" id="">
						<a onclick="document.tool.submit();" type="text"><h2>- Get Started - </h2></a>
					</form>
				</form>

				
			</div>	

            </div>


    </body>
</html>
