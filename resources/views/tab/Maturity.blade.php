<div>
	<h2 @click.prevent="MaturityTab='Maintainability'">Maintainability</h2>
	<div vv-show="MaturityTab=='Maintainability'">

		<!-- @todo move to conclusion -->
		<!-- <h3>Criteria</h3>
			<ul>
				<li>
					<div style="text-align:left;font-size:16px;margin:0;" class="explain">The OSS Project contains descriptive commits<span class="c_passed">[Passed or Failed ]</span></div>
				</li>
				<li>
					<div style="text-align:left;font-size:16px;margin:0;" class="explain">The OSS Project does not contain empty commits: <span class="c_passed">[Passed or Failed ]</span></div>
				</li>
	
			</ul> -->
			
		<table class="table">
			<thead>
				<th  style="width:400px;border-bottom:1px solid;">Commit Type</th>
				<th  style="border-bottom:1px solid;">Count</th>
			</thead>
			<tbody>
				<tr>
					<td>Fix</td>
					<td>@{{ MaturityScore.commits.fix.commits }} Commits</td>
				</tr>

				<tr>
					<td>Feature</td>
					<td>@{{ MaturityScore.commits.feature.commits }} Commits</td>
				</tr>

				<tr>
					<td>Uncategorized</td>
					<td>@{{ MaturityScore.commits.uncategorized.commits }} Commits</td>
					</tr>

					<tr>
					<td>Empty</td>
					<td>@{{ MaturityScore.commits.empty.commits }} Commits</td>
					</tr>

			</tbody>
		</table>
	</div>


	<ul>
		<li v-show="validPackage" class="ok">The project contains a valid package.json file</li>
		<li v-show="!validPackage" class="warn">The project does not contain a valid package.json file.</li>

		<li v-show="MaturityScore.containsBuildScripts" class="ok">The project contains build scripts to compile the source code.</li>
		<li v-show="!MaturityScore.containsBuildScripts" class="warn">The project does not contain build scripts.</li>

	</ul>
	<!-- Security -->
	<h2 @click.prevent="MaturityTab='Security'">Security</h2>
	<div vv-show="MaturityTab=='Security'">
		<p :class="{warn : MaturityScore.cve.length>0}">The tool has found @{{MaturityScore.cve.length}} related security incidents in the CVE database</p>
		<ul>
			<li v-for="elem in MaturityScore.cve"><a v-bind:href="'https://nvd.nist.gov/vuln/detail/' + elem.CVE " target="_blank">@{{elem.CVE}}</a>
				<ul>
					<li v-for="note, index in elem.Notes.Note">
					@{{index}}: @{{note}}
					</li>
				
				</ul>
			</li>
		</ul>
	</div>

	<h2 @click.prevent="MaturityTab='Support'">Support</h2>
	<div vv-show="MaturityTab=='Support'">
			<div :class="{warn: MaturityScore.support.count==0, ok:MaturityScore.support.count>0}">
				<span>@{{ MaturityScore.support.count }}</span> recent support topcics found on <a v-bind:href="'https://stackoverflow.com/search?q=' + repo.name">StackOverflow.com</a>
			</div>

			<ul :class="{warn: MaturityScore.support.count==0, ok:MaturityScore.support.count>0}">
				<li :class="{warn: MaturityScore.support.count==0, ok:MaturityScore.support.count>0}">The average support ticket has @{{ parseInt(MaturityScore.support.activity)}} Days of activity recorded</li>
				<li>From the @{{MaturityScore.support.count }} most recent questions
					<ul >
						<li>@{{MaturityScore.support.answered }} haved accepted answers</li>
						<li>@{{MaturityScore.support.withAnswers }} haved submitted, but not (yet) accepted answers</li>
					</ul>
				</li>
		
				<li>The most recent questions have @{{ MaturityScore.support.views }} views on average</li>
		
			</ul>

		<ul>
			
				<li :class="{warn: !repo.has_wiki, ok: repo.has_wiki}">The project <span v-show="repo.has_wiki">has</span><span v-show="!repo.has_wiki">has no</span> wiki</li>
				<li :class="{warn: !repo.has_pages, ok: repo.has_pages}">The project <span v-show="repo.has_pages">has</span><span v-show="!repo.has_pages">has no</span> pages</li>

		</ul>
		<ul :class="{warn: MaturityScore.collaborators.length==0, ok:MaturityScore.collaborators.length>0}">
			<li>@{{ MaturityScore.collaborators.length }} contributors found</li>
			<li>Top 30 Contributors (contributions)<br />
				<span v-for="(item, index) in MaturityScore.collaborators.slice(0,30)"><a target="_new" :href="item.html_url">@{{ item.login }}(@{{item.contributions}})</a>, 
			</span>
		</ul>
	</div>

	<h2>Documentation</h2>
	<ul>
		<li v-show="readme" class="ok">Readme file found</li>
		<li v-show="!readme" class="warn">No readme found!</li>

		<li v-show="external" class="ok">Contains external documentation</li>
		<li v-show="!external" class="warn">No external documentation found!</li>

		<li v-show="api" class="ok">API or option section found.</li>
		<li v-show="!api" class="warn">No API or option section found!</li>

		<li v-show="started" class="ok">Getting started or installation section found.</li>
		<li v-show="!started" class="warn">No getting started or installation section found!</li>

	</ul>
</div>