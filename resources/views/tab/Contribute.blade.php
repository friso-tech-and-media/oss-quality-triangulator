<div class="">
	<p>This short guide will help you getting started will the tool locally</p>

<pre>
	git clone https://gitlab.com/Arevico/oss-quality-triangulator ./oss/
	cd ./oss/
	npm install
	composer install
</pre>

	<p>
		Now, you can run the tool with
	</p>
<pre>
	php artisan serve
</pre>

<p>The tool is accessible on</p>
<pre>
	<a href="http://127.0.0.1:8000/">http://127.0.0.1:8000/</a>
</pre>
</div>