<div v-show="issues.length==0" class="warn">No issues detected! Make sure </div>

<i style="text-align:center;"> <span class="ok">Green</span> issues are solved or closed, <span class="warn">red</span> issues are open.</i> <br />&nbsp;</br />
<div v-for="issue in issues" :class="{warn:issue.state=='open',ok:issue.state=='closed'}">
	<div>[@{{issue.created_at }}]@{{ issue.title }}</div>

	<!-- <div v-show="openIssue==issue.id"> -->
<div style="color:#636b6f;">
	@{{ issue.body }}
	<div v-show="issue.body.length==0">&lt; no message &gt;</div>
<p>
	Last Updated: @{{ issue.updated_at }}
</p <br /> &nbsp;


</div>

	<!-- </div> -->
</div>