<div style="float:left;"><table>
<tr>
	<td style="width:200px;">Name</td>
	<td>@{{ repo.full_name }}</td>
</tr>

<tr>
	<td style="width:200px;">Description</td>
	<td>@{{ repo.description }}</td>
</tr>

<tr>
	<td style="width:200px;">Created</td>
	<td>@{{ repo.created_at }}</td>
</tr>
<tr>
	<td style="width:200px;">Update</td>
	<td>@{{ repo.updated_at }}</td>
</tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr>
	<td>is a fork?</td>
	<td>@{{ repo.fork }}</td>
</tr>

<tr>
	<td>Forks</td>
	<td>@{{ repo.forks }}</td>
</tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr>
	<td>Default Branch</td>
	<td>@{{ repo.default_branch }}</td>
</tr>

</table>
</div>
<div style="float:right;width:300px;">
	<p>
		User name: @{{ repo.owner.login }} - (@{{repo.owner.type}}) <br />	
	</p>
	<img style="width:250px" v-bind:src="repo.owner.avatar_url" alt="">
	<p>The author has associations with the following organisations: <br />
		<ul>
			<li v-for="(elem, index) in orgs">
			<i>@{{ elem.login  }}</i><br />
			@{{ elem.description}}<br />			
			</li>
		</ul>

	</p>
</div>
