<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	
// $Maturity = new App\MaturityScore();
// $sa 	  = App\StackExchange::create( 'laravel' );
// echo json_encode($sa->getQuestions(''));
// dd($sa->getStats());
// die();

Route::get('/', function () {return view('welcome');});

Route::get('/start/', 'Tool@Render')->name('tool');
Route::post('/feedback', 'Tool@feedback')->name('feedback');

// Route::get('/file-info/', 'Tool@Render')->name('tool');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
