<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App as App;
use \Cache;
use \Input;

class Tool extends Controller
{
	protected $files;
	
	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function xhrFileInfo(){

	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public  function render(){
		$cacheID = md5($_GET['url']);
		/**
		 * @var \App\Github
		 */
		$git = Cache::has("git_{$cacheID}") ? Cache::get("git_{$cacheID}") : new App\GitHub( $_GET['url']);

		$Maturity = new App\MaturityScore();
		$Maturity->categorizeCommits($git->getCommits());

		// dd($Maturity->getFiles($git) );
		// dd($Maturity->containsPackageJSON($git));
		//$packageJson = $git->repoQuery('contents/package.json');

		$se =  App\StackExchange::create($git->repoInfo->name);
		//dd($git->collaborators());
		
		$view 	= view('tool');
		
		$data 	= [
			'repo'		=> $git->repoInfo,
			'issues'	=> $git->getIssues(),
			'issues'	=> $git->getIssues(),
			'orgs' 		=> $git->getOrgs(),
			'commits' 	=> $git->getCommits(),
			'readme' 		=> !(empty($git->getReadmeFile() )),
			'validPackage'	=> $Maturity->containsPackageJSON($git),
			'MaturityScore' => [
				'commits' 	=> $Maturity->getCounts(),
				'cve' 	  	=> $Maturity->parseCVE($git->repo),
				'support' 	=> $se->getStats(),

				'containsBuildScripts' 	=> $Maturity->containsBuildScripts($git),
				'collaborators'	 		=> $git->collaborators(),
				'alternatives' 			=> $se->getAlternatives()				
			],
			
			'Risk' => [
				// Effort Estimation
				'testCases'	=> $Maturity->containsTestCases($git),
				'ci' 		=> $Maturity->containsCI($git),
				
				// Risk of having insufficient quality
				
				// Legal Risk
			],
						
			'external' 	=> $Maturity->externalDocumentation($git->getReadmeFile()),
			'api' 	=> $Maturity->apiSection($git->getReadmeFile()),
			'started' 	=> $Maturity->gettingStarted($git->getReadmeFile()),

			'openIssue' => 0,
		];

		// dd($se->getStats());
		
		$view->with('data', json_encode($data));
 
		Cache::add("git_{$cacheID}", $git, 60);
		
		return $view;
	}
	

	public function feedback(Request $request){
		$d = $request->input('data');
		array_unshift($d,$request->ip());
		unset($d['sent']);

		$fp = fopen('file.csv', 'a');
		fputcsv($fp, $d);

		fclose($fp);
	
		return 'Received';
	}
}

